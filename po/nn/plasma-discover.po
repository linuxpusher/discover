# Translation of plasma-discover to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2018, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-08-26 02:00+0000\n"
"PO-Revision-Date: 2023-07-27 21:20+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""
"Discover kan ikkje no brukast til å installera appar eller gjera system­"
"oppdateringar, då ingen av pakke­motorane er tilgjengelege."

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""
"Du kan installera nokre pakke­motorar i innstillingane, under sida "
"<interface>Manglande pakke­motorar</interface>.<nl/><nl/>Vurder òg å melda "
"frå om dette som ein pakkefeil i distribusjonen din."

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#, kde-format
msgid "Report This Issue"
msgstr "Meld frå om feilen"

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""
"Du kan bruka <command>pacman</command> for å installera valfrie avhengnadar "
"som er nødvendig for å ta i bruk pakke­motorane.<nl/><nl/>Merk at utviklarane "
"av Arch Linux tilrår å bruka <command>pacman</command> for pakke­handsaming, "
"då PackageKit-motoren ikkje er godt integrert på denne distroen."

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr "Lær meir"

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr "Fann ikkje kategorien «%1»"

# skip-rule: eksistera
#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "Prøver å opna den ikkje-eksisterande fila «%1»"

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"Kan ikkje bruka Flatpak-ressursar med mindre Flatpak-motoren %1 er "
"tilgjengeleg. Installer han først."

#: discover/DiscoverObject.cpp:310
#, kde-format
msgid "Could not open %1"
msgstr "Klarte ikkje opna %1"

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "Kontroller at Snap-støtte er installert"

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr ""
"Klarte ikkje opna %1, då programmet ikkje finst i nokon av pakkebrønnane."

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr "Meld frå om denne feilen til pakkarane av distribusjonen din."

# Namnet på programmet, som eg til slutt har funne ut at ikkje bør omsetjast. – KOH
#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:444
#: discover/main.cpp:121
#, kde-format
msgid "Discover"
msgstr "Discover"

#: discover/DiscoverObject.cpp:445
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""
"Discover vart lukka før alle oppgåvene var gjennomførte. Ventar på oppgåvene."

#: discover/main.cpp:42
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "Opna direkte det oppgjevne programmet med appstream://-adresssa."

#: discover/main.cpp:43
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr "Opna søk etter program som kan handtera den aktuelle MIME-typen."

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr "Vis ei liste over oppføringar med ein kategori."

#: discover/main.cpp:45
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr ""
"Opna Discover i vald modus. Modusar samsvarar med knappane på verktøylinja."

#: discover/main.cpp:46
#, kde-format
msgid "List all the available modes."
msgstr "Vis alle tilgjengelege modusar."

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr "Kompakt modus (auto/kompakt/full)."

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr "Lokal pakkefil som skal installerast"

#: discover/main.cpp:49
#, kde-format
msgid "List all the available backends."
msgstr "Vis alle tilgjengelege pakkemotorar."

#: discover/main.cpp:50
#, kde-format
msgid "Search string."
msgstr "Søkjetekst."

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Vis tilgjengelege val for tilbakemeldingar"

# skip-rule: url
#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "Støttar appstream: URL-format"

#: discover/main.cpp:123
#, kde-format
msgid "An application explorer"
msgstr "Ein programutforskar"

#: discover/main.cpp:125
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010–2022 Utviklingslaget for Plasma"

#: discover/main.cpp:126
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:127
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:128
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "Kvalitetskontroll, design og brukskvalitet"

#: discover/main.cpp:132
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "Dan Leinir Turthra Jensen"

#: discover/main.cpp:133
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:140
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer,Øystein Steffensen-Alværvik"

#: discover/main.cpp:140
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org,oysteins.omsetting@protonmail.com"

#: discover/main.cpp:153
#, kde-format
msgid "Available backends:\n"
msgstr "Tilgjengelege pakkemotorar:\n"

#: discover/main.cpp:209
#, kde-format
msgid "Available modes:\n"
msgstr "Tilgjengelege modusar:\n"

#: discover/qml/AddonsView.qml:19 discover/qml/navigation.js:43
#, kde-format
msgid "Addons for %1"
msgstr "Tillegg til %1"

#: discover/qml/AddonsView.qml:51
#, kde-format
msgid "More…"
msgstr "Meir …"

#: discover/qml/AddonsView.qml:60
#, kde-format
msgid "Apply Changes"
msgstr "Bruk innstillingane"

#: discover/qml/AddonsView.qml:67
#, kde-format
msgid "Reset"
msgstr "Nullstill"

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "Legg til nytt %1-depot"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "Legg til"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:269
#: discover/qml/InstallApplicationButton.qml:45
#: discover/qml/ProgressView.qml:106 discover/qml/SourcesPage.qml:190
#: discover/qml/UpdatesPage.qml:259 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "Avbryt"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:214
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 karakter"
msgstr[1] "%1 karakterar"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:214
#, kde-format
msgid "No ratings yet"
msgstr "Ingen karakterar enno"

#: discover/qml/ApplicationPage.qml:70
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr "Frå %1"

#: discover/qml/ApplicationPage.qml:81
#, kde-format
msgid "%1 - %2"
msgstr "%1 – %2"

#: discover/qml/ApplicationPage.qml:194
#, kde-format
msgid "Unknown author"
msgstr "Ukjend opphavsperson"

#: discover/qml/ApplicationPage.qml:238
#, kde-format
msgid "Version:"
msgstr "Versjon:"

#: discover/qml/ApplicationPage.qml:250
#, kde-format
msgid "Size:"
msgstr "Storleik:"

#: discover/qml/ApplicationPage.qml:262
#, kde-format
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "Lisens:"
msgstr[1] "Lisensar:"

#: discover/qml/ApplicationPage.qml:270
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "Ukjend"

#: discover/qml/ApplicationPage.qml:300
#, kde-format
msgid "What does this mean?"
msgstr "Kva tyder dette?"

#: discover/qml/ApplicationPage.qml:309
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "Vis fleire …"
msgstr[1] "Vis fleire …"

# Er snakk om OARS, altså i praksis aldersgrenser (med info om vald, nakenheit, språkbruk osv.).
#: discover/qml/ApplicationPage.qml:320
#, kde-format
msgid "Content Rating:"
msgstr "Aldersgrense:"

#: discover/qml/ApplicationPage.qml:329
#, kde-format
msgid "Age: %1+"
msgstr "Aldersgrense: %1+"

#: discover/qml/ApplicationPage.qml:349
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "Vis detaljar …"

#: discover/qml/ApplicationPage.qml:371
#, kde-format
msgctxt "@info placeholder message"
msgid "Screenshots not available for %1"
msgstr "Skjermbilete er ikkje tilgjengelege for %1"

#: discover/qml/ApplicationPage.qml:543
#, kde-format
msgid "Documentation"
msgstr "Dokumentasjon"

#: discover/qml/ApplicationPage.qml:544
#, kde-format
msgid "Read the project's official documentation"
msgstr "Les den offisielle dokumentasjonen til prosjektet"

#: discover/qml/ApplicationPage.qml:560
#, kde-format
msgid "Website"
msgstr "Heimeside"

#: discover/qml/ApplicationPage.qml:561
#, kde-format
msgid "Visit the project's website"
msgstr "Besøk heimesida til prosjektet"

#: discover/qml/ApplicationPage.qml:577
#, kde-format
msgid "Addons"
msgstr "Tillegg"

#: discover/qml/ApplicationPage.qml:578
#, kde-format
msgid "Install or remove additional functionality"
msgstr "Installer eller fjern tilleggsfunksjonalitet"

#: discover/qml/ApplicationPage.qml:597
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "Del"

#: discover/qml/ApplicationPage.qml:598
#, kde-format
msgid "Send a link for this application"
msgstr "Send lenkje til programmet"

#: discover/qml/ApplicationPage.qml:614
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr "Sjekk ut appen %1!"

#: discover/qml/ApplicationPage.qml:634
#, kde-format
msgid "What's New"
msgstr "Nytt i denne utgåva"

#: discover/qml/ApplicationPage.qml:664
#, kde-format
msgid "Reviews"
msgstr "Tilbakemeldingar"

#: discover/qml/ApplicationPage.qml:676
#, kde-format
msgid "Loading reviews for %1"
msgstr "Hentar vurderingar for %1"

#: discover/qml/ApplicationPage.qml:684
#, kde-format
msgctxt "@info placeholder message"
msgid "Reviews for %1 are temporarily unavailable"
msgstr "Vurderingar for %1 er mellombels utilgjengelege"

#: discover/qml/ApplicationPage.qml:712
#, kde-format
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "Vis %1 vurdering …"
msgstr[1] "Vis alle %1 vurderingane …"

#: discover/qml/ApplicationPage.qml:724
#, kde-format
msgid "Write a Review"
msgstr "Skriv ei vurdering"

#: discover/qml/ApplicationPage.qml:724
#, kde-format
msgid "Install to Write a Review"
msgstr "Installer for å skriva ei vurdering"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Get Involved"
msgstr "Bidra sjølv"

#: discover/qml/ApplicationPage.qml:778
#, kde-format
msgid "Donate"
msgstr "Gje pengegåve"

#: discover/qml/ApplicationPage.qml:779
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr "Støtt og takk utviklarane med ei pengegåve"

#: discover/qml/ApplicationPage.qml:795
#, kde-format
msgid "Report Bug"
msgstr "Meld frå om feil"

#: discover/qml/ApplicationPage.qml:796
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "Meld frå om feil du ønskjer retta"

#: discover/qml/ApplicationPage.qml:812
#, kde-format
msgid "Contribute"
msgstr "Bidra"

#: discover/qml/ApplicationPage.qml:813
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr "Hjelp utviklarane med programmering, design, testing eller omsetjing"

#: discover/qml/ApplicationPage.qml:838
#, kde-format
msgid "All Licenses"
msgstr "Alle lisensane"

# Er snakk om OARS, altså i praksis aldersgrenser (med info om vald, nakenheit, språkbruk osv.).
#: discover/qml/ApplicationPage.qml:871
#, kde-format
msgid "Content Rating"
msgstr "Aldersgrense"

#: discover/qml/ApplicationPage.qml:887
#, kde-format
msgid "Risks of proprietary software"
msgstr "Risiko ved proprietær programvare"

#: discover/qml/ApplicationPage.qml:893
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"Kjeldekoden til programmet er heilt eller delvis lukka for innsyn og "
"forbetring. Dette medfører at tredje­partar og brukarar som deg ikkje kan "
"stadfesta at programmet er trygt å bruka og fungerer slik det det skal. De "
"kan heller gjera endringar i programmet eller vidare­formidla det til andre "
"utan eksplisitt samtykke frå opphavspersonane.<nl/><nl/>Det kan vera at "
"programmet er heilt trygt å bruka, men det kan òg vera at det arbeider mot "
"deg – for eksempel ved å samla inn persondata, spora kor du oppheld deg "
"eller senda filene dine til andre. Det finst heller ikkje nokon enkel måte å "
"finna ut av dette på, så du bør berre installera programmet dersom du stolar "
"fullt og heilt på utviklarane bak (<link url='%1'>%2</link>).<nl/><nl/>Du "
"finn meir informasjon på <link url='%3'>%3</link>."

#: discover/qml/ApplicationPage.qml:894
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"Kjeldekoden til programmet er heilt eller delvis lukka for innsyn og "
"forbetring. Dette medfører at tredje­partar og brukarar som deg ikkje kan "
"stadfesta at programmet er trygt å bruka og fungerer slik det det skal. De "
"kan heller gjera endringar i programmet eller vidare­formidla det til andre "
"utan eksplisitt samtykke frå opphavspersonane.<nl/><nl/>Det kan vera at "
"programmet er heilt trygt å bruka, men det kan òg vera at det arbeider mot "
"deg – for eksempel ved å samla inn persondata, spora kor du oppheld deg "
"eller senda filene dine til andre. Det finst heller ikkje nokon enkel måte å "
"finna ut av dette på, så du bør berre installera programmet dersom du stolar "
"fullt og heilt på utviklarane bak (%1).<nl/><nl/>Du finn meir informasjon på "
"<link url='%2'>%2</link>."

#: discover/qml/ApplicationsListPage.qml:54
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "Søk: %2 – %3 element"
msgstr[1] "Søk: %2 – %3 element"

#: discover/qml/ApplicationsListPage.qml:56
#, kde-format
msgid "Search: %1"
msgstr "Søk: %1"

#: discover/qml/ApplicationsListPage.qml:60
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%2 – %1 element"
msgstr[1] "%2 – %1 element"

#: discover/qml/ApplicationsListPage.qml:66
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "Søk – %1 element"
msgstr[1] "Søk – %1 element"

#: discover/qml/ApplicationsListPage.qml:68
#: discover/qml/ApplicationsListPage.qml:235
#, kde-format
msgid "Search"
msgstr "Søk"

#: discover/qml/ApplicationsListPage.qml:89
#, kde-format
msgid "Sort: %1"
msgstr "Sortering: %1"

#: discover/qml/ApplicationsListPage.qml:93
#, kde-format
msgid "Name"
msgstr "Namn"

#: discover/qml/ApplicationsListPage.qml:103
#, kde-format
msgid "Rating"
msgstr "Karakter"

#: discover/qml/ApplicationsListPage.qml:113
#, kde-format
msgid "Size"
msgstr "Storleik"

#: discover/qml/ApplicationsListPage.qml:123
#, kde-format
msgid "Release Date"
msgstr "Utgjevingsdato"

#: discover/qml/ApplicationsListPage.qml:181
#, kde-format
msgid "Nothing found"
msgstr "Fann ingenting"

#: discover/qml/ApplicationsListPage.qml:189
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "Søk i alle kategoriane"

#: discover/qml/ApplicationsListPage.qml:199
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "Søk på nettet etter «%1»"

#: discover/qml/ApplicationsListPage.qml:203
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:214
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "Fann ikkje «%1» i kategorien «%2»"

#: discover/qml/ApplicationsListPage.qml:216
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "Fann ikkje «%1» i dei tilgjengelege kjeldene"

#: discover/qml/ApplicationsListPage.qml:217
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"Kan henda er «%1» tilgjengeleg frå nettet. Leverandøren din granskar ikkje "
"stabiliteten på eller funksjonane til program som vert lasta ned frå nettet. "
"Ver derfor varsam."

#: discover/qml/ApplicationsListPage.qml:250
#, kde-format
msgid "Still looking…"
msgstr "Leitar framleis …"

#: discover/qml/BrowsingPage.qml:20
#, kde-format
msgctxt "@title:window the name of a top-level 'home' page"
msgid "Home"
msgstr "Heim"

#: discover/qml/BrowsingPage.qml:63
#, kde-format
msgid "Unable to load applications"
msgstr "Klarte ikkje lasta programoversikt"

#: discover/qml/BrowsingPage.qml:101
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "Mest populære"

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "Redaktørens utvalde"

#: discover/qml/BrowsingPage.qml:138
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "Høgast vurderte spel"

#: discover/qml/BrowsingPage.qml:157 discover/qml/BrowsingPage.qml:186
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "Vis fleire"

#: discover/qml/BrowsingPage.qml:167
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "Høgast vurderte utviklarverktøy"

#: discover/qml/DiscoverWindow.qml:43
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "Å køyra som <em>rotbrukar</em> er unødvendig og sterkt frårådd."

#: discover/qml/DiscoverWindow.qml:56
#, kde-format
msgid "&Home"
msgstr "&Heim"

#: discover/qml/DiscoverWindow.qml:66
#, kde-format
msgid "&Search"
msgstr "&Søk"

#: discover/qml/DiscoverWindow.qml:74
#, kde-format
msgid "&Installed"
msgstr "&Installerte"

#: discover/qml/DiscoverWindow.qml:85
#, kde-format
msgid "Fetching &updates…"
msgstr "Hentar &oppdateringar …"

#: discover/qml/DiscoverWindow.qml:85
#, kde-format
msgid "&Update (%1)"
msgid_plural "&Updates (%1)"
msgstr[0] "&Oppdatering (%1)"
msgstr[1] "&Oppdateringar (%1)"

#: discover/qml/DiscoverWindow.qml:93
#, kde-format
msgid "&About"
msgstr "&Om"

#: discover/qml/DiscoverWindow.qml:101
#, kde-format
msgid "S&ettings"
msgstr "&Innstillingar"

#: discover/qml/DiscoverWindow.qml:154 discover/qml/DiscoverWindow.qml:338
#: discover/qml/DiscoverWindow.qml:445
#, kde-format
msgid "Error"
msgstr "Feil"

#: discover/qml/DiscoverWindow.qml:158
#, kde-format
msgid "Unable to find resource: %1"
msgstr "Finn ikkje ressurs: %1"

#: discover/qml/DiscoverWindow.qml:256 discover/qml/SourcesPage.qml:180
#, kde-format
msgid "Proceed"
msgstr "Hald fram"

#: discover/qml/DiscoverWindow.qml:314
#, kde-format
msgid "Report this issue"
msgstr "Meld frå om feilen"

#: discover/qml/DiscoverWindow.qml:338
#, kde-format
msgid "Error %1 of %2"
msgstr "Feil %1 av %2"

#: discover/qml/DiscoverWindow.qml:383
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "Vis førre"

#: discover/qml/DiscoverWindow.qml:396
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "Vis neste"

#: discover/qml/DiscoverWindow.qml:412
#, kde-format
msgid "Copy to Clipboard"
msgstr "Kopier til utklippstavla"

#: discover/qml/Feedback.qml:13
#, kde-format
msgid "Submit usage information"
msgstr "Send inn bruksinformasjon"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"Sender anonymisert bruksinformasjon til KDE, slik at me kan forstå brukarane "
"våre betre. For meir informasjon, sjå https://kde.org/privacypolicy-apps.php."

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Submitting usage information…"
msgstr "Sender inn bruksinformasjon …"

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Configure"
msgstr "Set opp"

#: discover/qml/Feedback.qml:22
#, kde-format
msgid "Configure feedback…"
msgstr "Set opp tilbakemeldingar …"

#: discover/qml/Feedback.qml:29 discover/qml/SourcesPage.qml:21
#, kde-format
msgid "Configure Updates…"
msgstr "Set opp oppdateringar …"

#: discover/qml/Feedback.qml:57
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"Du kan hjelpa oss med å gjera programmet betre ved å dela bruksstatistikk og "
"delta i spørjeundersøkingar."

#: discover/qml/Feedback.qml:57
#, kde-format
msgid "Contribute…"
msgstr "Bidra …"

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "We are looking for your feedback!"
msgstr "Me ønskjer tilbakemelding frå deg!"

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "Participate…"
msgstr "Delta …"

#: discover/qml/InstallApplicationButton.qml:24
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "Lastar …"

#: discover/qml/InstallApplicationButton.qml:28
#, kde-format
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "Installer frå %1"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "Installer"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgid "Remove"
msgstr "Fjern"

#: discover/qml/InstalledPage.qml:15
#, kde-format
msgid "Installed"
msgstr "Installerte"

#: discover/qml/navigation.js:18
#, kde-format
msgid "Resources for '%1'"
msgstr "Ressursar for «%1»"

#: discover/qml/ProgressView.qml:16
#, kde-format
msgid "Tasks (%1%)"
msgstr "Oppgåver (%1 %)"

#: discover/qml/ProgressView.qml:16 discover/qml/ProgressView.qml:41
#, kde-format
msgid "Tasks"
msgstr "Oppgåver"

#: discover/qml/ProgressView.qml:99
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 – %2: %3, %4 att"

#: discover/qml/ProgressView.qml:100
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 – %2: %3"

#: discover/qml/ProgressView.qml:101
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 – %2"

#: discover/qml/ReviewDelegate.qml:64
#, kde-format
msgid "unknown reviewer"
msgstr "ukjend vurderar"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1:</b> av %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "Comment by %1"
msgstr "Kommentar frå %1"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "Version: %1"
msgstr "Versjon: %1"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "Version: unknown"
msgstr "Versjon: ukjend"

#: discover/qml/ReviewDelegate.qml:98
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "Røyster: %1 av %2"

#: discover/qml/ReviewDelegate.qml:105
#, kde-format
msgid "Was this review useful?"
msgstr "Var denne vurderinga nyttig?"

#: discover/qml/ReviewDelegate.qml:117
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "Ja"

#: discover/qml/ReviewDelegate.qml:134
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "Nei"

#: discover/qml/ReviewDialog.qml:19
#, kde-format
msgid "Reviewing %1"
msgstr "Vurderer %1"

#: discover/qml/ReviewDialog.qml:25
#, kde-format
msgid "Submit review"
msgstr "Send inn vurdering"

#: discover/qml/ReviewDialog.qml:38
#, kde-format
msgid "Rating:"
msgstr "Karakter:"

#: discover/qml/ReviewDialog.qml:43
#, kde-format
msgid "Name:"
msgstr "Namn:"

#: discover/qml/ReviewDialog.qml:51
#, kde-format
msgid "Title:"
msgstr "Tittel:"

#: discover/qml/ReviewDialog.qml:69
#, kde-format
msgid "Enter a rating"
msgstr "Skriv inn vurdering"

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgid "Write the title"
msgstr "Skriv tittel"

#: discover/qml/ReviewDialog.qml:75
#, kde-format
msgid "Write the review"
msgstr "Skriv vurdering"

#: discover/qml/ReviewDialog.qml:78
#, kde-format
msgid "Keep writing…"
msgstr "Skriv vidare …"

#: discover/qml/ReviewDialog.qml:81
#, kde-format
msgid "Too long!"
msgstr "For langt!"

#: discover/qml/ReviewDialog.qml:84
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "Set inn namn"

#: discover/qml/ReviewsPage.qml:44
#, kde-format
msgid "Reviews for %1"
msgstr "Vurderingar av %1"

#: discover/qml/ReviewsPage.qml:55
#, kde-format
msgid "Write a Review…"
msgstr "Skriv ei vurdering …"

#: discover/qml/ReviewsPage.qml:60
#, kde-format
msgid "Install this app to write a review"
msgstr "Installer programmet for å skriva ei vurdering"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search…"
msgstr "Søk …"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search in '%1'…"
msgstr "Søk i «%1» …"

#: discover/qml/SourcesPage.qml:17
#, kde-format
msgid "Settings"
msgstr "Innstillingar"

#: discover/qml/SourcesPage.qml:98
#, kde-format
msgid "Default source"
msgstr "Standardkjelde"

#: discover/qml/SourcesPage.qml:105
#, kde-format
msgid "Add Source…"
msgstr "Legg til kjelde …"

#: discover/qml/SourcesPage.qml:131
#, kde-format
msgid "Make default"
msgstr "Gjer til standard"

#: discover/qml/SourcesPage.qml:222
#, kde-format
msgid "Increase priority"
msgstr "Høgare prioritet"

#: discover/qml/SourcesPage.qml:228
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "Klarte ikkje auka prioriteten for «%1»"

#: discover/qml/SourcesPage.qml:234
#, kde-format
msgid "Decrease priority"
msgstr "Lågare prioritet"

#: discover/qml/SourcesPage.qml:240
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "Klarte ikkje senka prioriteten for «%1»"

#: discover/qml/SourcesPage.qml:246
#, kde-format
msgid "Remove repository"
msgstr "Fjern pakkebrønn"

#: discover/qml/SourcesPage.qml:257
#, kde-format
msgid "Show contents"
msgstr "Vis innhald"

# Sjå tilsvarande feilmeldingar.
#: discover/qml/SourcesPage.qml:297
#, kde-format
msgid "Missing Backends"
msgstr "Manglande pakkemotorar"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "Oppdateringar"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "Oppdateringsproblem"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr "Tekniske detaljar"

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr ""
"Det oppstod ein feil ved installering av oppdateringa. Prøv igjen seinare."

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr "Vis tekniske detaljar"

#: discover/qml/UpdatesPage.qml:94
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid ""
"If the error indicated above looks like a real issue and not a temporary "
"network error, please report it to %1, not KDE."
msgstr ""
"Viss feilen ovanfor verkar å vera eit verkeleg problem, ikkje ein mellombels "
"nettverksfeil, meld frå om han til %1, ikkje til KDE."

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Copy Text"
msgstr "Kopier tekst"

#: discover/qml/UpdatesPage.qml:106
#, kde-format
msgctxt "@info %1 is the name of the user's distro/OS"
msgid "Error message copied. Remember to report it to %1, not KDE!"
msgstr ""
"Feilmeldinga vart kopiert til utklippstavla. Hugs å melda frå om feilen til "
"%1, ikkje til KDE."

#: discover/qml/UpdatesPage.qml:113
#, kde-format
msgctxt "@action:button %1 is the name of the user's distro/OS"
msgid "Report Issue to %1"
msgstr "Meld frå om feilen til %1"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update the selected items' "
msgid "Update Selected"
msgstr "Oppdater valde"

#: discover/qml/UpdatesPage.qml:140
#, kde-format
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "Oppdater alle"

#: discover/qml/UpdatesPage.qml:181
#, kde-format
msgid "Ignore"
msgstr "Ignorer"

#: discover/qml/UpdatesPage.qml:227
#, kde-format
msgid "Select All"
msgstr "Merk alle"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Select None"
msgstr "Fjern merking"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "Start på nytt automatisk etter oppdateringa"

#: discover/qml/UpdatesPage.qml:249
#, kde-format
msgid "Total size: %1"
msgstr "Total storleik: %1"

#: discover/qml/UpdatesPage.qml:284
#, kde-format
msgid "Restart Now"
msgstr "Start på nytt"

#: discover/qml/UpdatesPage.qml:384
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:400
#, kde-format
msgid "Installing"
msgstr "Installerer"

#: discover/qml/UpdatesPage.qml:436
#, kde-format
msgid "Update from:"
msgstr "Oppdater frå:"

#: discover/qml/UpdatesPage.qml:448
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:455
#, kde-format
msgid "More Information…"
msgstr "Meir informasjon …"

#: discover/qml/UpdatesPage.qml:483
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "Hentar oppdateringar …"

#: discover/qml/UpdatesPage.qml:496
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "Oppdateringar"

#: discover/qml/UpdatesPage.qml:505
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "Maskina må startast på nytt for å fullføra oppdateringa."

#: discover/qml/UpdatesPage.qml:517 discover/qml/UpdatesPage.qml:524
#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "À jour"

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "Skal sjå etter oppdateringar"

#: discover/qml/UpdatesPage.qml:545
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "Tid for siste oppdatering er ukjend"
